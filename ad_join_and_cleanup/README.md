# Ansible Role: ad_join_and_cleanup

This role automates the process of joining Windows VMs to an Active Directory domain, transitioning from a local WinRM account to a domain-based service account, and rotating the service account password. It is designed for large-scale environments and leverages advanced Ansible features such as **block/rescue/always**, **handlers**, and **batching (serial)**. This README serves as a lab guide to help you learn how to use Ansible roles and their advanced features.

---

## Table of Contents

- [Overview](#overview)
- [Features](#features)
- [Directory Structure](#directory-structure)
- [Role Variables](#role-variables)
- [Task Breakdown](#task-breakdown)
- [Error Handling and Rollback](#error-handling-and-rollback)
- [Using Batching for Large Fleets](#using-batching-for-large-fleets)
- [Dynamic Inventory Integration](#dynamic-inventory-integration)
- [Usage in AWX](#usage-in-awx)
- [Testing and Security Considerations](#testing-and-security-considerations)
- [License](#license)

---

## Overview

The `ad_join_and_cleanup` role performs the following actions:

1. **Join Windows VMs to Active Directory**  
   Uses the `win_domain_membership` module to join VMs to the specified AD domain. If necessary, reboots the VM to finalize the domain join.

2. **Transition from Local to Domain Account**  
   Adds a new domain service account (used for automation) to the local Administrators group via the `win_group_membership` module and removes the old local WinRM account with the `win_user` module.

3. **Password Rotation**  
   Contains a placeholder task for rotating the domain service account password. This can be extended to integrate with a secret management system (e.g., Azure Key Vault or CyberArk).

4. **Error Handling & Rollback**  
   Uses Ansible’s **block/rescue/always** constructs to catch errors at each phase. A rollback handler is notified on failure to undo changes (e.g., leaving the domain and restoring the local account).

5. **Batching**  
   Implements the **serial** keyword in the playbook to process hosts in manageable batches, reducing risk when applying changes to a large fleet.

---

## Features

- **Block/Rescue/Always**:  
  Each critical operation (domain join, service account update, password rotation) is wrapped in a block. If a block fails, the rescue section triggers, notifying a rollback handler, and the always section logs completion for auditing purposes.

- **Handlers for Rollback**:  
  A dedicated handler (`rollback_changes`) is defined to reverse changes if an error is encountered. This ensures that the system can return to a known state.

- **Batched Execution**:  
  When executing the playbook, the `serial` keyword allows you to process a subset of VMs at a time, reducing the impact of any failures.

- **Dynamic Inventory Integration**:  
  A sample dynamic inventory file using the Azure RM plugin groups Windows VMs automatically. This demonstrates how to leverage dynamic inventory plugins to target specific groups of hosts.

- **AWX Integration**:  
  The role includes job tags for each phase, allowing for granular orchestration in AWX (e.g., running only domain join tasks or just the password rotation block).

---

## Directory Structure

The `ad_join_and_cleanup` role has the following directory structure:

- `ad_join_and_cleanup/`
  - `defaults/`
    - `main.yml` - Default variables for the role
  - `files/`
    - `some_file.txt` - Example file used by the role
  - `handlers/`
    - `main.yml` - Handlers for rollback and other notifications
  - `meta/`
    - `main.yml` - Role metadata
  - `tasks/`
    - `main.yml` - Main task file
    - `domain_join.yml` - Tasks for joining the domain
    - `account_transition.yml` - Tasks for transitioning to domain account
    - `password_rotation.yml` - Tasks for rotating the password
  - `templates/`
    - `some_template.j2` - Example template used by the role
  - `tests/`
    - `test.yml` - Test playbook for the role
  - `vars/`
    - `main.yml` - Variables for the role
  - `README.md` - Documentation for the role


- **defaults/main.yml:**  
  Contains default variables such as `ad_domain`, `ad_ou`, and credential placeholders.

- **handlers/main.yml:**  
  Defines rollback logic that attempts to revert the domain join, restore the local account, and remove the new domain account from the local Administrators group if something goes wrong.

- **tasks/main.yml:**  
  Organizes tasks into logical blocks (domain join, service account addition, and password rotation) with error handling (`block/rescue/always`) and tagging for selective execution.

---

## Role Variables

Key variables defined in `defaults/main.yml` include:

- **ad_domain:**  
  The fully-qualified domain name of your Active Directory (e.g., `example.com`).

- **ad_ou:**  
  The Organizational Unit (OU) path where computer accounts should reside (e.g., `OU=Servers,DC=example,DC=com`).

- **domain_join_user & domain_join_pass:**  
  Credentials for a domain account with permissions to join computers to the domain.

- **domain_service_account & domain_service_password:**  
  The new domain-based service account that will be used for WinRM access. Replace placeholders with secure lookups in production.

- **old_local_account:**  
  The existing local WinRM account that will be removed post-migration.

*Note:* Always replace any hard-coded sensitive data with secure retrieval methods such as Ansible Vault or external secret management systems (e.g., Azure Key Vault or CyberArk).

---

## Task Breakdown

### 1. Domain Join and Reboot Block

- **Join VM to AD:**  
  Uses `ansible.windows.win_domain_membership` to join the VM to the AD domain.

- **Reboot if Needed:**  
  Checks the result and reboots the VM using `ansible.windows.win_reboot` if necessary.

- **Tags:** `domain_join`, `reboot`

### 2. Service Account Addition and Local Account Removal Block

- **Add Service Account:**  
  Uses `win_group_membership` to add the new domain service account to the local Administrators group.

- **Connectivity Test:**  
  Uses `win_ping` (delegated to localhost) to verify connectivity using the new account.

- **Remove Local Account:**  
  Uses `win_user` to remove the old local WinRM account.

- **Tags:** `add_service_account`, `test_connectivity`, `remove_local_user`

### 3. Password Rotation Block

- **Rotate Password:**  
  Contains a placeholder task using `win_shell` to demonstrate where password rotation logic should be implemented.

- **Tag:** `password_rotation`

Each block is wrapped in `block/rescue/always` to handle errors and trigger the rollback if necessary.

---

## Error Handling and Rollback

- **Block/Rescue/Always:**  
  Each critical operation is enclosed in a `block`. If any task fails within that block, the `rescue` section will run to log the error and notify the rollback handler.

- **Rollback Handler:**  
  Defined in `handlers/main.yml`, it attempts to:
  - Reverse the domain join (leave the domain).
  - Recreate the old local WinRM account.
  - Remove the new domain service account from the local Administrators group.

  This mechanism is designed to revert the system to a safe state in the event of failure.

---

## Batched Execution for Large Fleets

For large-scale deployments, apply changes in manageable batches to minimize risk. A sample playbook using the **serial** keyword is provided below:

```yaml
---
- name: Domain Join and Account Transition for Windows VMs
  hosts: azure_windows_vms
  serial: 10           # Process 10 hosts at a time; adjust this number as needed.
  gather_facts: no     # Set to yes if fact gathering is required.
  roles:
    - ad_join_and_cleanup
