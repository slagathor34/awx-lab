```mermaid
graph LR
    start["HTTP GET /virtual-machines"]
    env -->|Retrieve COSMOS_ENDPOINT and COSMOS_KEY from environment variables| client[CosmosClient]
    client -->|Get my-db database| database["my-db database"]
    database -->|Get my-container container| container["my-container container"]
    container -->|Query virtual machine names| query["SELECT c.name FROM c WHERE c.type = 'virtual-machine'"]
    query -->|Return list of virtual machine names| response[["List of virtual machine names"]]
    start -->|Trigger function| env
    env -->|Authenticate with Cosmos DB| client
    client -->|Retrieve database| database
    database -->|Retrieve container| container
    container -->|Execute SQL query| query
    query -->|Process results| response
```

## Desired State Pre Bake
```mermaid 
sequenceDiagram 
  Image Gallery->>Create MotherDoughVM: Create VM Bakery base
  Create MotherDoughVM->>Apply Desired State: Conform VM to desired state
  Apply Desired State->>Create MotherDoughVM: Finish Setup
  Create MotherDoughVM->>Power Off VM: Set up for Generalization
  Power Off VM->>Generalize VM: Prepare the image for use
  Generalize VM-->>Image Gallery: Store the Generalized VM in the Gallery for use
```

## Desired State VM Post Bake
```mermaid 
sequenceDiagram 
  Image Gallery->>Create MotherDoughVM: Create VM Bakery base
  Create MotherDoughVM->>Apply Desired State: Conform VM to desired state
  Apply Desired State->>Create MotherDoughVM: Finish Setup
  Create MotherDoughVM->>Power Off VM: Set up for Generalization
  Power Off VM->>Generalize VM: Prepare the image for use
  Generalize VM-->>Image Gallery: Store the Generalized VM in the Gallery for use
  Image Gallery->> Scale Set: Scale Set takes the new Generalized VM and begins rotating images into production based on rules
  Scale Set->>Desired State Configuration Application: Each VM executes a pull or push request to apply Desired State before being added to the Load Balancer
  Desired State Configuration Application->>Update production load blanacer: Bring VM to service
```

## Desired State Container Bakery
```mermaid
sequenceDiagram
  Azure Container Registry->>Create MotherDough Container: Create Container Bakery base
  Create MotherDough Container->>Apply Desired State: Conform container to desired state and security vulnerabiity scan
  Apply Desired State->>Create MotherDough Container: Finish Setup
  Create MotherDough Container->>Push to Azure Container Registry: Push to registry for consumption
  Azure Container Registry-->>Azure Kubernetes/ACI/Azure App Service Env: Pull identified code when submitted to the registry
```

## Requirements
- [ ] Provide a VM that all services are started and ready for full utilization on start up 
- [ ] Image of the Virtual Machine that can be generalized for use in a Scale Set
- [ ] Update process that can be used to update 

## VM Image Gallery

```code
---
- name: Create shared image gallery
  azure_rm_gallery:
    resource_group: "{{ rg_name }}"
    name: "{{ shared_gallery_name }}"
    location: "{{ region }}"
    description: "{{ gallery_description }}"
  tags: [CreateSharedImageGallery]
```

## Create Mother Dough VM
```code
---
- name: Create public IP address
  azure_rm_publicipaddress:
    resource_group: "{{ rg_name }}"
    allocation_method: Static
    name: "{{ vm_name }}-{{ subnet_name }}myPublicIP"
    sku: "Standard"
  register: output_ip_address
  tags: [CreateMotherDoughVM]

- name: Create virtual network interface card
  azure_rm_networkinterface:
    resource_group: "{{ rg_name }}"
    name: "{{ vm_name }}-{{ subnet_name }}nic"
    virtual_network: "{{ vnet_name }}"
    subnet: "App-name-{{ subnet_name }}"
    security_group: "App-name-{{ subnet_name }}"
    ip_configurations:
      - public_ip_name: "{{ vm_name }}-{{ subnet_name }}myPublicIP"
        name: "{{ vm_name }}-{{ subnet_name }}IP"
        primary: True
  tags: [CreateMotherDoughVM]

- name: create Azure storage account
  azure_rm_storageaccount:
    name: myvm0985645
    resource_group: "{{ rg_name }}"
    account_type: Standard_LRS
    minimum_tls_version: TLS1_2
  tags: [CreateMotherDoughVM]

- name: Create Windows VM
  when: ostype == "windows"
  azure_rm_virtualmachine:
    resource_group: "{{ rg_name }}"
    name: "{{ vm_name }}-{{ subnet_name }}"
    vm_size: "{{ vmsize }}"
    admin_username: azureadmin
    admin_password: "{{ admin_password }}"
    network_interfaces: "{{ vm_name }}-{{ subnet_name }}nic"
    availability_set: "avs-managed-disk{{ subnet_name }}"
    managed_disk_type: Standard_LRS
    vm_identity: SystemAssigned
    os_type: Windows
    image:
      offer: WindowsServer
      publisher: MicrosoftWindowsServer
      sku: "2022-datacenter-core-g2"
      version: latest
    storage_account_name: myvm0985645
    virtual_network_name: "{{ vnet_name }}"
    tags:
      AoC: "{{ subnet_name }}"
  tags: [CreateMotherDoughVM]

- name: create Azure vm extension to enable HTTPS WinRM listener
  when: ostype == "windows"
  azure_rm_virtualmachineextension:
    name: winrm-extension
    resource_group: "{{ rg_name }}"
    virtual_machine_name: "{{ vm_name }}-{{ subnet_name }}"
    publisher: Microsoft.Compute
    virtual_machine_extension_type: CustomScriptExtension
    type_handler_version: "1.9"
    settings: '{"commandToExecute": "powershell.exe -ExecutionPolicy ByPass -EncodedCommand {{winrm_enable_script}}"}'
    #settings: '{"fileUris": ["https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1"],"commandToExecute": "powershell -ExecutionPolicy Unrestricted -File ConfigureRemotingForAnsible.ps1"}'
    auto_upgrade_minor_version: true
  tags: [CreateMotherDoughVM]

- name: Create CentOS VM
  when: ostype == "centos"
  azure_rm_virtualmachine:
    resource_group: "{{ rg_name }}"
    name: "{{ vm_name }}-{{ subnet_name }}"
    vm_size: "{{ vmsize }}"
    admin_username: azureadmin
    admin_password: "{{ admin_password }}"
    network_interfaces: "{{ vm_name }}-{{ subnet_name }}nic"
    availability_set: "avs-managed-disk{{ subnet_name }}"
    managed_disk_type: Standard_LRS
    vm_identity: SystemAssigned
    os_type: Linux
    image:
      offer: CentOS
      publisher: OpenLogic
      sku: "7.5"
      version: latest
    storage_account_name: myvm0985645
    virtual_network_name: "{{ vnet_name }}"
    tags:
      AoC: "{{ subnet_name }}"
  tags: [CreateMotherDoughVM]
```

## Apply Desired State
```code
---
- name: Install Windows Feature Set
  win_dsc:
    resource_name: WindowsFeature
    Name: "{{ item }}"
    Ensure: Present
  with_items:
    - 'Web-Server'
    - 'Web-Mgmt-Console'
    - 'Windows-Server-Backup'
    - 'Web-Asp-Net45'
  tags: InstallDSCWinFeature

- name: Create IIS Website with Binding and Authentication options
  win_dsc:
    resource_name: xWebsite
    Ensure: Present
    Name: DSC Website
    State: Started
    PhysicalPath: C:\inetpub\wwwroot
    BindingInfo: # Example of a CimInstance[] DSC parameter (list of dicts)
    - Protocol: http
      Port: 80
      IPAddress: '*'
    AuthenticationInfo: # Example of a CimInstance DSC parameter (dict)
      Anonymous: no
      Basic: true
      Digest: false
      Windows: yes
  tags: ConfigureIISServer
```

## Power Off VM
```code
---
- hosts: localhost
  connection: local
  tasks:
    - name: Power Off
      azure_rm_virtualmachine:
        resource_group: "{{ rg_name }}"
        name: "{{ vm_name }}{{ item.name }}{{ item.zone }}{{ subnet_name }}"
        started: no
```

## Generalize VM
```code
---
- name: Generalize VM
  azure_rm_virtualmachine:
    resource_group: "{{ rg_name }}"
    name: "{{ vm_name }}"
    generalized: yes
  tags: [GeneralizeVM]
```

## Create Shared Image
```code
---
- name: Create custom image
  azure_rm_image:
    resource_group: "{{ rg_name }}"
    name: "{{ image_name }}"
    source: "{{ vm_name }}"
  tags: [CreateCustomImage]

- name: Create shared image
  azure_rm_galleryimage:
    resource_group: "{{ rg_name }}"
    gallery_name: "{{ shared_gallery_name }}"
    name: "{{ shared_image_name }}"
    location: "{{ region }}"
    os_type: "{{ ostype }}"
    os_state: generalized
    identifier:
        publisher: "{{ publisher_name }}"
        offer: "{{ offer_name }}"
        sku: "{{ sku }}"
    description: "{{ image_description }}"
  tags: [CreateSharedImage]

- name: Create or update a simple gallery image version.
  azure_rm_galleryimageversion:
    resource_group: "{{ rg_name }}"
    gallery_name: "{{ shared_gallery_name }}"
    gallery_image_name: "{{ shared_image_name }}"
    name: "{{ shared_image_version }}"
    location: "{{ region }}"
    publishing_profile:
        end_of_life_date: "2024-10-01t00:00:00+00:00"
        exclude_from_latest: yes
        replica_count: 3
        storage_account_type: Standard_LRS
        target_regions:
        - name: West US
          regional_replica_count: 1
        - name: East US
          regional_replica_count: 2
          storage_account_type: Standard_ZRS
        managed_image:
        name: "{{ image_name }}"
        resource_group: "{{ rg_name }}"
    register: output
  tags: [CreateOrUpdateGalleryImageVersion]
```

## Azure Scale Set Configuration
```mermaid 
sequenceDiagram 
  Pattern #1->>Scale Set: Create Base Pattern
  Scale Set->>Custom Image: Use Custom Image from the Shared Image Gallery
```


## Create Scale Set from Custom Image
```code
---
- name: Create a scale set
      azure_rm_virtualmachinescaleset:
        resource_group: "{{ rg_name }}"
        name: "{{ vmss_name }}"
        vm_size: Standard_DS1_v2
        admin_username: "{{ admin_username }}"
        admin_password: "{{ admin_password }}"
        ssh_password_enabled: true
        capacity: 2
        virtual_network_name: "{{ vm_name }}"
        subnet_name: "{{ vm_name }}"
        upgrade_policy: Manual
        tier: Standard
        managed_disk_type: Standard_LRS
        os_disk_caching: ReadWrite
        image:
          name: "{{ image_name }}" # Name of the Image in the Image Gallery
          resource_group: "{{ rg_gallery_name }}" # Name of the Resource Group with the Image Gallery
        load_balancer: "{{ vmss_name }}lb"
```

## Auto Scale by CPU Utilization Metrics
```code
---
- hosts: localhost
  vars:
    rg_name: myResourceGroup
    vmss_name: myScaleSet
    name: autoscalesetting
  tasks:
  - name: Get facts of the resource group
    azure_rm_resourcegroup_facts:
      name: "{{ rg_name }}"
    register: rg

  - name: Get scale set resource uri
    set_fact:
      vmss_id: "{{ rg.ansible_facts.azure_resourcegroups[0].id }}/providers/Microsoft.Compute/virtualMachineScaleSets/{{ vmss_name }}"
    
  - name: Create autoscaling
    azure_rm_autoscale:
      resource_group: "{{ rg_name }}"
      name: "{{ name }}"
      target: "{{ vmss_id }}"
      enabled: true
      profiles:
      - count: '1'
        max_count: '1'
        min_count: '1'
        name: 'This scale condition is executed when none of the other scale condition(s) match'
        recurrence_days:
          - Monday
        recurrence_frequency: Week
        recurrence_hours:
          - 18
        recurrence_mins:
          - 0
        recurrence_timezone: Pacific Standard Time
      - count: '1'
        min_count: '1'
        max_count: '4'
        name: Auto created scale condition
        recurrence_days:
          - Monday
        recurrence_frequency: Week
        recurrence_hours:
          - 18
        recurrence_mins:
          - 0
        recurrence_timezone: Pacific Standard Time
        rules:
          - cooldown: 5
            direction: Increase
            metric_name: Percentage CPU
            metric_resource_uri: "{{ vmss_id }}"
            operator: GreaterThan
            statistic: Average
            threshold: 70
            time_aggregation: Average
            time_grain: 1
            time_window: 10
            type: ChangeCount
            value: '1'
          - cooldown: 5
            direction: Decrease
            metric_name: Percentage CPU
            metric_resource_uri: "{{ vmss_id }}"
            operator: LessThan
            statistic: Average
            threshold: 30
            time_aggregation: Average
            time_grain: 1
            time_window: 10
            type: ChangeCount
            value: '1'
```

Steps for update image reference of scale set –

1.  Create new VM using image which got created (myCustomImage) while deploying json template (you might noticed this is Managed Disk VM)
2.  Update changes or make configuration changes to this VM because this VM going to be uses as new GOLD image for scale set.
3.  Once you done with changes again Generalize VM from OS side.Go to Run and Type “sysprep”, Generalize the VM and shutdown.
4.  Run below PowerShell command to deallocate the VM.Stop-AzureRmVm -ResourceGroupName “RGNAME” -Name “VMname” -Force
5.  Because it’s managed disk VM you can capture the image from Portal only.  
    Remember- Before clicking on Capture make sure you have generalized the VM, once you click on Capture that VM will not come up again.
6.  Run “Get-AzureRmImage” in PowerShell you will see image name which you captured in steps 5, take the ID of that image.
7.  Run Below PowerShell command to update image reference of scale set  
    $rgname = “RGname”  
    $vmssname = “Scalesetname”  
    $newImageReference = “ID of new image captured in steps 6″# get the VMSS model  
    $vmss = Get-AzureRmVmss -ResourceGroupName $rgname -VMScaleSetName $vmssname# set the new version in the model data  
    $vmss.virtualMachineProfile.storageProfile.imageReference.id = $newImageReference# update the virtual machine scale set model  
    Update-AzureRmVmss -ResourceGroupName $rgname -Name $vmssname -VirtualMachineScaleSet $vmssAbove command will update image reference of scale set but running instance running with old image, you need to update those instance with new image.
8.  To update running Instance with New Image, you need to go to scale sets settings-> Instances–> Select that instance which showing Latest Model as NO and click on upgrade.  
    Note- Make sure you have other instance which running service in scale sets because during upgrade VM will be created again with new image.
