![process flow](/img/awx-container-groups/SCR-20232202-oflu.png)
1. AWX on premise is configured with a Kubernetes Credential, token, and certificate
1. Requests are made into the cluster on TCP 6443 into the AKS cluster 
1. AKS cluster spins up a pod with the execution environment and executes the playbooks with the provided credentials. In this case, it uses the Managed identity assigned to the pod that the AzureADIdentity pod intercepts 
1. Pod creates the Azure Resources
1. Resource Group is created by the Pod
1. vNet is created by the Pod

```
kubectl create namespace awx

kubectl create serviceaccount awx-api

kubectl create 

kubectl create clusterrole awx-api-remote —verb=‘*’ —resource=‘*’ 

kubectl create clusterrolebinding awx-api --role=‘awx-api-remote’ 

clusterrole=awx-api-remote --serviceaccount=awx:awx-api —account=default:awx-api 

kubectl create token awx-api -n awx

```
