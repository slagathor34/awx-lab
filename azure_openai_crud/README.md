# Azure OpenAI CRUD Role

This Ansible role, **azure_openai_crud**, provides full CRUD (Create, Read, Update, Delete) operations for managing an Azure OpenAI deployment using the `azure_rm_resource` module. The role is designed to minimize repetition by centralizing common parameters and splitting operations into individual task files. Each CRUD operation is implemented with robust error handling using block/rescue/always constructs, assertions to verify outcomes, and a rollback mechanism in case of failure.

## Features

- **CRUD Operations:** Create, read, update, and delete an Azure OpenAI deployment.
- **Job Tags:** Each operation is tagged (`create`, `read`, `update`, `delete`) for flexible execution.
- **Error Handling:** Uses block/rescue/always constructs with assertions to ensure that each step succeeds and to trigger rollbacks when necessary.
- **Rollback Notification:** A handler notifies when a rollback is triggered.

## Directory Structure

```
roles/azure_openai_crud/
├── defaults
│   └── main.yml          # Default variables including Azure subscription, resource group, and network settings.
├── handlers
│   └── main.yml          # Handler for rollback notifications.
└── tasks
    ├── create.yml        # Tasks to create the deployment.
    ├── delete.yml        # Tasks to delete the deployment.
    ├── main.yml          # Main task file including all CRUD operation tasks.
    ├── read.yml          # Tasks to read deployment details.
    └── update.yml        # Tasks to update the deployment.
```

## Default Variables

Defined in `defaults/main.yml`, these variables include:

- **subscription_id:** Your Azure subscription ID.
- **resource_group:** The Azure resource group name.
- **account_name:** The Azure OpenAI account name.
- **deployment_name:** The deployment name under the account.
- **location:** The Azure region (e.g., `northcentralus`).
- **vnet_subnet_id:** The full resource ID for the VNet subnet to restrict access.
- **private_endpoint_id:** The full resource ID for the private endpoint.
- **azure_openai_resource:** A consolidated dictionary for common resource parameters (API version, resource group, provider, resource type, resource name, and location).

## Usage

1. **Place the Role in Your Roles Directory**

   Ensure the role is placed under your `roles/` directory in your Ansible project.

2. **Include the Role in Your Playbook**

   Create a playbook (e.g., `site.yml`) similar to the following:

   ```yaml
   ---
   - name: Run Azure OpenAI CRUD role
     hosts: localhost
     connection: local
     gather_facts: no
     roles:
       - role: azure_openai_crud
   ```

3. **Execute Specific Operations**

   Use the `--tags` option to run specific CRUD operations. For example, to run only the create operation:

   ```bash
   ansible-playbook site.yml --tags create
   ```

   Replace `create` with `read`, `update`, or `delete` to execute the corresponding tasks.

## Error Handling and Rollback

- **Block/Rescue/Always Constructs:**  
  Each CRUD operation is enclosed in a block to handle errors. If an error occurs, the rescue block attempts to rollback the changes (usually by deleting the resource).

- **Assertions:**  
  After key tasks, assertions verify that the operations have succeeded.

- **Rollback Notification:**  
  The handler `rollback_notification` is triggered if a rollback occurs, ensuring you are notified of the failure.

## Customization

- **Overriding Variables:**  
  You can override any default variable in your inventory, playbook, or via extra vars to tailor the role to your Azure environment.

- **Task Modifications:**  
  If additional customizations are needed for your deployment, you can modify the individual task files (`create.yml`, `read.yml`, `update.yml`, `delete.yml`).

## Prerequisites

- **Ansible Version:**  
  Ansible 2.9+ is recommended.

- **Azure Credentials:**  
  Ensure the `azure_rm_resource` module is configured with appropriate credentials. Authentication can be set up via environment variables or an Ansible credential file.

- **Dependencies:**  
  The role depends on the Azure modules provided by Ansible for resource management.

## Author

Sonny Stormes
