# Azure Data Factory CRUD Role

This Ansible role, **azure_data_factory_crud**, provides full CRUD (Create, Read, Update, Delete) operations for managing an Azure Data Factory instance with private networking and private endpoint configuration. It leverages the `azure_rm_datafactory` and `azure_rm_datafactory_info` modules following the standard module format.

## Features

- **CRUD Operations:** Create, read, update, and delete an Azure Data Factory instance.
- **Private Networking:** Configures the Data Factory with a managed virtual network and private endpoints.
- **Job Tags:** Each operation is tagged (`create`, `read`, `update`, `delete`) for flexible execution.
- **Error Handling & Rollback:** Implements block/rescue/always constructs with assertions to verify success and automatically roll back in case of failure.

## Directory Structure

```
roles/azure_data_factory_crud/
├── defaults
│   └── main.yml          # Default variables: subscription details, resource group, Data Factory name, and networking settings.
├── handlers
│   └── main.yml          # Handler for rollback notifications.
└── tasks
    ├── create.yml        # Tasks for creating the Data Factory.
    ├── delete.yml        # Tasks for deleting the Data Factory.
    ├── main.yml          # Main task file that includes all CRUD operation tasks.
    ├── read.yml          # Tasks for reading Data Factory details.
    └── update.yml        # Tasks for updating the Data Factory.
```

## Default Variables

Defined in `defaults/main.yml`, these variables include:

- **subscription_id:** Your Azure subscription ID.
- **resource_group:** The name of the resource group.
- **data_factory_name:** The name of the Data Factory instance.
- **location:** The Azure region where the Data Factory is deployed (e.g., `eastus`).
- **private_link_resource_id:** The full resource ID for the private link service.
- **private_endpoint_name:** The name to assign to the private endpoint.
- **azure_data_factory_resource:** A consolidated dictionary containing the API version, resource group, Data Factory name, and location.

## Prerequisites

- **Ansible Version:** Recommended version 2.9+.
- **Azure Credentials:** Ensure necessary Azure credentials are configured (via environment variables or an Ansible credential file).
- **Required Modules:** This role depends on the `azure_rm_datafactory` and `azure_rm_datafactory_info` modules from the `azure.azcollection`.

## Usage

1. **Install the Role**

   Place the role in your `roles/` directory within your Ansible project.

2. **Include the Role in a Playbook**

   Create a playbook (e.g., `site.yml`) similar to the following:

   ```yaml
   ---
   - name: Manage Azure Data Factory
     hosts: localhost
     connection: local
     gather_facts: no
     roles:
       - role: azure_data_factory_crud
   ```

3. **Run Specific Operations**

   Use the `--tags` option to execute a specific CRUD operation. For example, to run only the create operation:

   ```bash
   ansible-playbook site.yml --tags create
   ```

   Replace `create` with `read`, `update`, or `delete` to perform the respective operations.

## Error Handling and Rollback

- **Block/Rescue/Always Constructs:**  
  Each CRUD operation is encapsulated in a block to manage errors. If an error occurs, the rescue block will roll back changes (for example, by deleting the Data Factory).

- **Assertions:**  
  Assertions validate that each operation succeeds by verifying response statuses and expected resource properties.

- **Rollback Notification:**  
  A handler (`rollback_notification`) is triggered if a rollback is initiated, ensuring you are notified of any failures.

## Customization

- **Overriding Variables:**  
  You can override any default variable in your inventory, playbook, or via extra vars to customize the role to your Azure environment.

- **Task Adjustments:**  
  Modify the individual task files (`create.yml`, `read.yml`, `update.yml`, `delete.yml`) if additional customizations or steps are needed.

## Author

Sonny Stormes