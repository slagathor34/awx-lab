# azure_kv_crud Role

The **azure_kv_crud** role provides complete CRUD (Create, Read, Update, Delete) operations on Azure Key Vault resources: secrets, keys, and certificates. Designed for robust automation in enterprise environments, this role leverages advanced Ansible features including block/rescue/always constructs, job tags for orchestration in AWX, and handlers to ensure changes are applied correctly—with rollback on failures.

---

## Features

- **Complete CRUD Operations:**  
  Perform create, read, update, and delete actions on Azure Key Vault secrets, keys, and certificates.

- **Job Tags:**  
  Each operation is tagged (e.g., `secrets_create`, `keys_update`, `certs_delete`) so that you can orchestrate individual steps in AWX workflows or run targeted operations using the `--tags` option.

- **Advanced Error Handling:**  
  Uses Ansible's block/rescue/always structure to catch errors and trigger a rollback handler if any CRUD block fails.

- **Rollback Mechanism:**  
  A dedicated handler (`rollback_changes`) is notified on failures to allow for manual or automated rollback, ensuring the system remains in a consistent state.

- **Reduced Code Repetition:**  
  Leverages loops and a modular structure to minimize code duplication and simplify maintenance.

## Directory Structure

- **defaults/main.yml:**  
  Contains default variables such as `keyvault_uri`, `secrets_list`, `keys_list`, and `certificates_list`. Replace placeholder values with secure lookups in production.

- **handlers/main.yml:**  
  Defines rollback logic to revert changes if something goes wrong during execution.

- **tasks/main.yml:**  
  Organizes tasks into logical blocks with error handling (using block/rescue/always) and tags for selective execution.

## Role Variables

The default variables are defined in `defaults/main.yml`:

- **keyvault_uri:**  
  The URI of your Azure Key Vault (e.g., `https://myKeyVault.vault.azure.net`).

- **secrets_list:**  
  A list of secret names to perform CRUD operations on.

- **keys_list:**  
  A list of key names to perform CRUD operations on.

- **certificates_list:**  
  A list of certificate names to perform CRUD operations on.

You can override these defaults in your inventory or playbook as needed.

## Task Overview

### Secrets CRUD Operations

- **Create:**  
  Creates a secret with an initial value.  
  *Tag:* `secrets_create`

- **Read:**  
  Retrieves and verifies the existence of the secret.  
  *Tag:* `secrets_read`

- **Update:**  
  Updates the secret with a new generated value.  
  *Tag:* `secrets_update`

- **Delete:**  
  Deletes the secret from the Key Vault.  
  *Tag:* `secrets_delete`

### Keys CRUD Operations

- **Create:**  
  Creates a new key (e.g., an RSA key).  
  *Tag:* `keys_create`

- **Read:**  
  Retrieves the key for verification.  
  *Tag:* `keys_read`

- **Update:**  
  Updates the key (for example, by changing key size or other properties).  
  *Tag:* `keys_update`

- **Delete:**  
  Deletes the key from the Key Vault.  
  *Tag:* `keys_delete`

### Certificates CRUD Operations

- **Create:**  
  Creates a certificate based on a defined policy (e.g., self-signed with a validity period).  
  *Tag:* `certs_create`

- **Read:**  
  Retrieves the certificate for verification.  
  *Tag:* `certs_read`

- **Update:**  
  Updates the certificate (e.g., renewing it or modifying properties).  
  *Tag:* `certs_update`

- **Delete:**  
  Deletes the certificate from the Key Vault.  
  *Tag:* `certs_delete`

Each CRUD block is wrapped in a block/rescue/always construct to manage errors and trigger rollback if necessary.

## Error Handling and Rollback

- **Block/Rescue/Always:**  
  Each critical operation is enclosed in a block. If a failure occurs, the rescue section logs the error and triggers the rollback handler.

- **Rollback Handler:**  
  Defined in `handlers/main.yml`, it attempts to:
  - Reverse any changes (if applicable).
  - Restore the previous state.
  
  This ensures that the system returns to a known safe state if something goes wrong.

## Usage

### Running the Role Directly

Create a playbook (e.g., `launch.yml`) that includes this role:

```
name: Run Azure Key Vault CRUD Operations 
hosts: localhost 
connection: local 
gather_facts: no roles:
azure_kv_crud
```


Run the playbook with:

```
ansible-playbook launch.yml
```

### Targeting Specific Operations

To run specific operations, use the `--tags` option. For example, to run only the create operations:

```
ansible-playbook launch.yml --tags "secrets_create,keys_create,certs_create"
```

### Integration with AWX

In AWX, create job templates that reference this role. Utilize the job tags to control which operations are executed. For example, you might create separate job templates for creation, updating, or deletion operations, then chain them together in a workflow.

## Prerequisites

- **Azure.azcollection:**  
  Ensure the azure.azcollection collection is installed on your control node.

- **Azure Authentication:**  
  Configure Azure credentials (via environment variables, a credentials file, or Managed Identity) so that Ansible can authenticate with Azure.

- **Permissions:**  
  Your account must have permissions to perform CRUD operations on the specified Key Vault.

## License

This role is distributed under the MIT License. See the [LICENSE](LICENSE) file for more details.

This role serves as a comprehensive example of managing Azure Key Vault resources with Ansible. It demonstrates best practices in error handling, rollback, job tagging, and modular role design, making it an excellent resource for automating secure key management tasks in Azure.